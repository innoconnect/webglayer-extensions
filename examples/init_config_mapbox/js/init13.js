function init() {
	const conf = new WGL.ui.Config("app2.json", '../../../webglayer/', 'map', "right");
	conf.setAfterLoadFunction(function () {
    map.on("move", onMove);

    $("#webglayer").css("z-index","1");
    $("#webglayer").css("display","none");

    const layer = {
      "id": "canvas",
      "source": "canvas",
      "type": "raster",
      "paint": { 'raster-fade-duration': 0 }
    };

    const layers = map.getStyle().layers;
    // Find the index of the first symbol layer in the map style
    let firstSymbolId;
    for (let i = 0; i < layers.length; i++) {
      if (layers[i].type === 'symbol') {
        firstSymbolId = layers[i].id;
        break;
      }
    }
    const map_source = {
      "type": 'canvas',
      "canvas": 'webglayer',
      "coordinates": [
        [-12, 60],
        [2, 60],
        [2, 50],
        [-12, 50]
      ],
      "animate": true
    };
    map.addSource("canvas", map_source);
    map.addLayer(layer, firstSymbolId);

    onMove();

  });
	conf.load();
  //setInterval(time,5000);
}
	
function getTopLeftTC() {

	var tlwgs = (new OpenLayers.LonLat(-180, 90)).transform(
			new OpenLayers.Projection("EPSG:4326"),
		 	new OpenLayers.Projection("EPSG:900913"));
	
	var s = Math.pow(2, map.getZoom());
	tlpixel = map.getViewPortPxFromLonLat(tlwgs);
	res = {
			x : -tlpixel.x / s,
			y : -tlpixel.y / s
	};
	//console.log(res);
	return res;
}
	
function onMove() {
		WGL.mcontroller.zoommove(map.getZoom(), getTopLeftTC(), WGL.filterByExt);
}

function time() {
	console.time();
  map.setZoom(map.getZoom()+1);
  //map.setZoom(map.getZoom()-1);
	//WGL.render();
	console.timeEnd();
}

/**
 * Compute coordinates of top-left conner in zero level pixel
 * for MapBox JS GL API
 * Note: zoom = mapbox_zoom + 1 !!!!
 * @returns {{x: number, y: number}}
 */
function getTopLeftTC() {
  const ZERO_PIX_3857_COEF = 128/20037508.34;
  const z = map.getZoom() + 1;
  const scale = Math.pow(2, z);
  const dx = WGL.getManager().w/2/scale;
  const dy = WGL.getManager().h/2/scale;

  const TL3857_ZERO = {x: -20037508.34, y: 20037508.34};
  const c = map.getCenter();

  const proj = new SphericalMercator.SphericalMercator();
  const center_3857 = proj.forward([c.lng, c.lat]);

  return {
    x: (center_3857[0] - TL3857_ZERO.x)*ZERO_PIX_3857_COEF - dx,
    y: (-center_3857[1] + TL3857_ZERO.y)*ZERO_PIX_3857_COEF - dy
  };
}

function onMove() {
  const z = map.getZoom() + 1;
  WGL.mcontroller.zoommove(z, getTopLeftTC());
  modifyCanvasCor();
}

function modifyCanvasCor(){
  const b = map.getBounds();
  let cor = [];
  cor[0] = [b._sw.lng, b._ne.lat];
  cor[1] = [b._ne.lng, b._ne.lat];
  cor[2] = [b._ne.lng, b._sw.lat];
  cor[3] = [b._sw.lng, b._sw.lat];
  let canvas_source = map.getSource('canvas');
  canvas_source.setCoordinates(cor);
  canvas_source.play();
  canvas_source.prepare();
  //canvas_source.pause();
}

function wgsToZeroLevel(pos){
  const proj = new SphericalMercator.SphericalMercator();
  const point = proj.forward(pos);
  const x = (point[0] + 20037508.34) / (20037508.34*2)*256;
  const y = -(point[1] - 20037508.34) / (20037508.34*2)*256;
  return {x: x, y: y}
}
